int ThermistorPin = 0;
int Vo;
float R1 = 10000;
float logR2, R2, T, Tc, Tf;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

void setup() {
Serial.begin(9600);
for(int pinNumber=2; pinNumber<5; pinNumber++){
  pinMode(pinNumber, OUTPUT);
  digitalWrite(pinNumber, LOW);
}
}

void loop() {

  Vo = analogRead(ThermistorPin);
  float Vt = Vo * (5.0/1023.0);
  R2 = R1 * (1023/ (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  Tc = T - 273.15;

  Serial.print("Voltage: ");
  Serial.print(Vt);
  Serial.print(" Temperature: ");
  Serial.print(Tc);
  Serial.println(" C");   
  /*Light Up Certain LEDs
  Blue: 4
  Green: 3
  Red: 2
  */
  if(Tc<25){
    digitalWrite(4, HIGH);
    digitalWrite(3, LOW);
    digitalWrite(2, LOW);
  }else if(Tc<50){
    digitalWrite(4, LOW);
    digitalWrite(3, HIGH);
    digitalWrite(2, LOW);
  }else{
    digitalWrite(4, LOW);
    digitalWrite(3, LOW);
    digitalWrite(2, HIGH);
  }
  delay(500);
  
}

